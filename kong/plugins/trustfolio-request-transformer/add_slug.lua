local set_uri_args = kong.service.request.set_raw_query
local get_consumer = kong.client.get_consumer
local DEBUG = ngx.DEBUG

-- new table
local _M = {}

function _M.add(conf)
  -- get query to add
  local query = conf.add

  -- get consumer
  local consumer = get_consumer()
  kong.log(DEBUG, "consumer identified ", consumer.username)
  local consumer_slug = consumer.custom_id

  -- build querystring
  local start = ("query=query{profile(slug:\""..consumer_slug.."\")")
  local querystring = (start .. query)
  kong.log(DEBUG, "added querystring ", querystring)
  set_uri_args(querystring)
end

return _M