-- Import our class for removing sensitive headers
local add_slug = require "kong.plugins.trustfolio-request-transformer.add_slug"

-- Import the base kong plugin
local BasePlugin = require "kong.plugins.base_plugin"

-- Extend our plugin from the base plugin
local TrustfolioRequestTransformerHandler = BasePlugin:extend()

-- Setting this very early to avoid logging any service-tokens
TrustfolioRequestTransformerHandler.PRIORITY = 5

-- creates a new instance of the plugin
function TrustfolioRequestTransformerHandler:new()
  TrustfolioRequestTransformerHandler.super.new(self, "trustfolio-request-transformer")
end

-- plugin built-in method to handle request query add
function TrustfolioRequestTransformerHandler:access(conf)
  TrustfolioRequestTransformerHandler.super.access(self)
  -- Add our logic to add consumer slug
  add_slug.add(conf)
end

-- return the plugin class
return TrustfolioRequestTransformerHandler